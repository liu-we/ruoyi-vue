import request from '@/utils/request'

// 查询热害防治措施列表
export function listPreventMeasure(query) {
  return request({
    url: '/coalMine/preventMeasure/list',
    method: 'get',
    params: query
  })
}

// 查询热害防治措施详细
export function getPreventMeasure(measureId) {
  return request({
    url: '/coalMine/preventMeasure/' + measureId,
    method: 'get'
  })
}

// 新增热害防治措施
export function addPreventMeasure(data) {
  return request({
    url: '/coalMine/preventMeasure',
    method: 'post',
    data: data
  })
}

// 修改热害防治措施
export function updatePreventMeasure(data) {
  return request({
    url: '/coalMine/preventMeasure',
    method: 'put',
    data: data
  })
}

// 删除热害防治措施
export function delPreventMeasure(measureId) {
  return request({
    url: '/coalMine/preventMeasure/' + measureId,
    method: 'delete'
  })
}
//
// // 修改措施状态
//
// export function changeMeasureStatus(measureId, status) {
//   const data = {
//     measureId,
//     status
//   }
//   return request({
//     url: '/coalMine/changeStatus/',
//     method:'put',
//     data:data
//   })
// }
