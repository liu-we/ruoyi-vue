package com.ruoyi.coalMine.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.utils.SecurityUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.coalMine.domain.PreventMeasure;
import com.ruoyi.coalMine.service.IPreventMeasureService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;
import org.springframework.web.multipart.MultipartFile;

/**
 * 热害防治措施Controller
 *
 * @author LiuWe
 * @date 2023-06-26
 */
@RestController
@RequestMapping("/coalMine/preventMeasure")
public class PreventMeasureController extends BaseController
{
    @Autowired
    private IPreventMeasureService preventMeasureService;

    /**
     * 查询热害防治措施列表
     */
    @PreAuthorize("@ss.hasPermi('coalMine:preventMeasure:list')")
    @GetMapping("/list")
    public TableDataInfo list(PreventMeasure preventMeasure)
    {
        startPage();
        List<PreventMeasure> list = preventMeasureService.selectPreventMeasureList(preventMeasure);
        return getDataTable(list);
    }

    /**
     * 导出热害防治措施列表
     */
    @PreAuthorize("@ss.hasPermi('coalMine:preventMeasure:export')")
    @Log(title = "热害防治措施", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, PreventMeasure preventMeasure)
    {
        List<PreventMeasure> list = preventMeasureService.selectPreventMeasureList(preventMeasure);
        ExcelUtil<PreventMeasure> util = new ExcelUtil<PreventMeasure>(PreventMeasure.class);
        util.exportExcel(response, list, "热害防治措施数据");
    }

    /**
     * 获取热害防治措施详细信息
     */
    @PreAuthorize("@ss.hasPermi('coalMine:preventMeasure:query')")
    @GetMapping(value = "/{measureId}")
    public AjaxResult getInfo(@PathVariable("measureId") Long measureId)
    {
        return success(preventMeasureService.selectPreventMeasureByMeasureId(measureId));
    }

    /**
     * 新增热害防治措施
     */
    @PreAuthorize("@ss.hasPermi('coalMine:preventMeasure:add')")
    @Log(title = "热害防治措施", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody PreventMeasure preventMeasure)
    {
        preventMeasure.setCreateBy(SecurityUtils.getUsername());
        return toAjax(preventMeasureService.insertPreventMeasure(preventMeasure));
    }

    /**
     * 修改热害防治措施
     */
    @PreAuthorize("@ss.hasPermi('coalMine:preventMeasure:edit')")
    @Log(title = "热害防治措施", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody PreventMeasure preventMeasure)
    {
        preventMeasure.setUpdateBy(SecurityUtils.getUsername());
        return toAjax(preventMeasureService.updatePreventMeasure(preventMeasure));
    }

    /**
     * 删除热害防治措施
     */
    @PreAuthorize("@ss.hasPermi('coalMine:preventMeasure:remove')")
    @Log(title = "热害防治措施", businessType = BusinessType.DELETE)
    @DeleteMapping("/{measureIds}")
    public AjaxResult remove(@PathVariable Long[] measureIds)
    {
        return toAjax(preventMeasureService.deletePreventMeasureByMeasureIds(measureIds));
    }

    /**
     * 导入数据
     * @param file
     * @param updateSupport
     * @return
     * @throws Exception
     */
    @Log(title = "热害防治措施", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('coalMine:preventMeasure:importData')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) throws Exception
    {
        ExcelUtil<PreventMeasure> util = new ExcelUtil<PreventMeasure>(PreventMeasure.class);
        List<PreventMeasure> dataList = util.importExcel(file.getInputStream());
        String operName = SecurityUtils.getUsername();
        String message = preventMeasureService.importData(dataList, updateSupport, operName);
        return AjaxResult.success(message);
    }



    @PostMapping("/importTemplate")
    public void importTemplate(HttpServletResponse response)
    {
        ExcelUtil<PreventMeasure> util = new ExcelUtil<PreventMeasure>(PreventMeasure.class);
        util.importTemplateExcel(response, "热害防治措施数据");
    }
}
