package com.ruoyi.coalMine.service.impl;

import java.util.List;

import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.exception.ServiceException;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.bean.BeanValidators;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.coalMine.mapper.PreventMeasureMapper;
import com.ruoyi.coalMine.domain.PreventMeasure;
import com.ruoyi.coalMine.service.IPreventMeasureService;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Validator;

/**
 * 热害防治措施Service业务层处理
 *
 * @author LiuWe
 * @date 2023-06-26
 */
@Service
public class PreventMeasureServiceImpl implements IPreventMeasureService
{
    @Autowired
    private PreventMeasureMapper preventMeasureMapper;

    @Autowired
    protected Validator validator;

    /**
     * 查询热害防治措施
     *
     * @param measureId 热害防治措施主键
     * @return 热害防治措施
     */
    @Override
    public PreventMeasure selectPreventMeasureByMeasureId(Long measureId)
    {
        return preventMeasureMapper.selectPreventMeasureByMeasureId(measureId);
    }

    /**
     * 查询热害防治措施列表
     *
     * @param preventMeasure 热害防治措施
     * @return 热害防治措施
     */
    @Override
    public List<PreventMeasure> selectPreventMeasureList(PreventMeasure preventMeasure)
    {
        return preventMeasureMapper.selectPreventMeasureList(preventMeasure);
    }

    /**
     * 新增热害防治措施
     *
     * @param preventMeasure 热害防治措施
     * @return 结果
     */
    @Override
    public int insertPreventMeasure(PreventMeasure preventMeasure)
    {
        preventMeasure.setCreateTime(DateUtils.getNowDate());
        return preventMeasureMapper.insertPreventMeasure(preventMeasure);
    }

    /**
     * 修改热害防治措施
     *
     * @param preventMeasure 热害防治措施
     * @return 结果
     */
    @Override
    public int updatePreventMeasure(PreventMeasure preventMeasure)
    {
        preventMeasure.setUpdateTime(DateUtils.getNowDate());
        return preventMeasureMapper.updatePreventMeasure(preventMeasure);
    }

    /**
     * 批量删除热害防治措施
     *
     * @param measureIds 需要删除的热害防治措施主键
     * @return 结果
     */
    @Override
    public int deletePreventMeasureByMeasureIds(Long[] measureIds)
    {
        return preventMeasureMapper.deletePreventMeasureByMeasureIds(measureIds);
    }

    /**
     * 删除热害防治措施信息
     *
     * @param measureId 热害防治措施主键
     * @return 结果
     */
    @Override
    public int deletePreventMeasureByMeasureId(Long measureId)
    {
        return preventMeasureMapper.deletePreventMeasureByMeasureId(measureId);
    }

    @Override
    public String importData(List<PreventMeasure> dataList, boolean updateSupport, String operName) {
        if (StringUtils.isNull(dataList) || dataList.size() == 0)
        {
            throw new ServiceException("导入数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (PreventMeasure data : dataList)
        {
            try
            {
                // 验证是否存在这个数据
                PreventMeasure p = preventMeasureMapper.selectPreventMeasureByMeasureTitle(data.getMeasureTitle());
                if (StringUtils.isNull(p))
                {
                    BeanValidators.validateWithException(validator, data);
                    data.setCreateBy(operName);
                    preventMeasureMapper.insertPreventMeasure(data);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、标题 " + data.getMeasureTitle() + " 导入成功");
                }
                else if (updateSupport)
                {
                    BeanValidators.validateWithException(validator, data);
                    data.setUpdateBy(operName);
                    preventMeasureMapper.updatePreventMeasure(data);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、标题 " + data.getMeasureTitle() + " 更新成功");
                }
                else
                {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、标题 " + data.getMeasureTitle() + " 已存在");
                }
            }
            catch (Exception e)
            {
                failureNum++;
                String msg = "<br/>" + failureNum + "、标题 " + data.getMeasureTitle() + " 导入失败：";
                failureMsg.append(msg + e.getMessage());
            }
        }
        if (failureNum > 0)
        {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条标题格式不正确，错误如下：");
            throw new ServiceException(failureMsg.toString());
        }
        else
        {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }
}

