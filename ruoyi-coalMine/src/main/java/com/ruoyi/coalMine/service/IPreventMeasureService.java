package com.ruoyi.coalMine.service;

import java.util.List;
import com.ruoyi.coalMine.domain.PreventMeasure;
import com.ruoyi.common.core.domain.entity.SysUser;

import javax.servlet.http.HttpServletResponse;

/**
 * 热害防治措施Service接口
 *
 * @author LiuWe
 * @date 2023-06-26
 */
public interface IPreventMeasureService
{
    /**
     * 查询热害防治措施
     *
     * @param measureId 热害防治措施主键
     * @return 热害防治措施
     */
    public PreventMeasure selectPreventMeasureByMeasureId(Long measureId);

    /**
     * 查询热害防治措施列表
     *
     * @param preventMeasure 热害防治措施
     * @return 热害防治措施集合
     */
    public List<PreventMeasure> selectPreventMeasureList(PreventMeasure preventMeasure);

    /**
     * 新增热害防治措施
     *
     * @param preventMeasure 热害防治措施
     * @return 结果
     */
    public int insertPreventMeasure(PreventMeasure preventMeasure);

    /**
     * 修改热害防治措施
     *
     * @param preventMeasure 热害防治措施
     * @return 结果
     */
    public int updatePreventMeasure(PreventMeasure preventMeasure);

    /**
     * 批量删除热害防治措施
     *
     * @param measureIds 需要删除的热害防治措施主键集合
     * @return 结果
     */
    public int deletePreventMeasureByMeasureIds(Long[] measureIds);

    /**
     * 删除热害防治措施信息
     *
     * @param measureId 热害防治措施主键
     * @return 结果
     */
    public int deletePreventMeasureByMeasureId(Long measureId);

    String importData(List<PreventMeasure> dataList, boolean updateSupport, String operName);
}
