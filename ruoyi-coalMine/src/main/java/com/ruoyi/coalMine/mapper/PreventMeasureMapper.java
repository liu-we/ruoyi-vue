package com.ruoyi.coalMine.mapper;

import java.util.List;
import com.ruoyi.coalMine.domain.PreventMeasure;

import javax.servlet.http.HttpServletResponse;

/**
 * 热害防治措施Mapper接口
 *
 * @author LiuWe
 * @date 2023-06-26
 */
public interface PreventMeasureMapper
{
    /**
     * 查询热害防治措施
     *
     * @param measureId 热害防治措施主键
     * @return 热害防治措施
     */
    public PreventMeasure selectPreventMeasureByMeasureId(Long measureId);

    /**
     * 查询热害防治措施列表
     *
     * @param preventMeasure 热害防治措施
     * @return 热害防治措施集合
     */
    public List<PreventMeasure> selectPreventMeasureList(PreventMeasure preventMeasure);

    /**
     * 新增热害防治措施
     *
     * @param preventMeasure 热害防治措施
     * @return 结果
     */
    public int insertPreventMeasure(PreventMeasure preventMeasure);

    /**
     * 修改热害防治措施
     *
     * @param preventMeasure 热害防治措施
     * @return 结果
     */
    public int updatePreventMeasure(PreventMeasure preventMeasure);

    /**
     * 删除热害防治措施
     *
     * @param measureId 热害防治措施主键
     * @return 结果
     */
    public int deletePreventMeasureByMeasureId(Long measureId);

    /**
     * 批量删除热害防治措施
     *
     * @param measureIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePreventMeasureByMeasureIds(Long[] measureIds);

    PreventMeasure selectPreventMeasureByMeasureTitle(String measureTitle);
}
