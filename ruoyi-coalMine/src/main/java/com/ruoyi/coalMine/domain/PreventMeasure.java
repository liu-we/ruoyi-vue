package com.ruoyi.coalMine.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.util.Date;

/**
 * 热害防治措施对象 heat_prevention_measure
 *
 * @author LiuWe
 * @date 2023-06-26
 */
public class PreventMeasure extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 措施ID */
    @Excel(name = "措施ID")
    private Long measureId;

    /** 措施标题 */
    @Excel(name = "措施标题")
    private String measureTitle;

    /** 措施内容 */
    @Excel(name = "措施内容")
    private String measureContent;

    @Excel(name = "创建者")
    private String createBy;
    @Excel(name = "创建日期")
    private Date createDate;
    @Excel(name = "备注")
    private String remark;
    @Excel(name = "更新者")
    private String updateBy;
    @Excel(name = "更新日期")
    private Date updateDate;

    @Override
    public String getUpdateBy() {
        return updateBy;
    }

    @Override
    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    @Override
    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }


    @Override
    public String getCreateBy() {
        return createBy;
    }

    @Override
    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }




    public void setMeasureId(Long measureId)
    {
        this.measureId = measureId;
    }

    public Long getMeasureId()
    {
        return measureId;
    }
    public void setMeasureTitle(String measureTitle)
    {
        this.measureTitle = measureTitle;
    }

    public String getMeasureTitle()
    {
        return measureTitle;
    }

    public void setMeasureContent(String measureContent)
    {
        this.measureContent = measureContent;
    }

    public String getMeasureContent()
    {
        return measureContent;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("measureId", getMeasureId())
                .append("measureTitle", getMeasureTitle())
                .append("measureContent", getMeasureContent())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .append("remark", getRemark())
                .toString();
    }
}
